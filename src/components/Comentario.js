import React from 'react';
import './Comentario.css'
import userPng from './user.png'

const Comentario = props => {
    return (
        <div className="Comentario">
            <img src={userPng} className="avatar" alt={props.nome}/>
            <div className="conteudo">
                <h2 className="nome">{props.nome}</h2>    
                <p className="email">{props.email}</p>
                <div className="mensagem">{props.mensagem}</div>
                <div className="data">{props.data.toString()}</div>
                <button onClick={props.onRemove}>&times;</button>
            </div>
        </div>
    )
}

export default Comentario;