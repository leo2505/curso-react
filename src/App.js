import { Component } from 'react';
import './App.css';
import Comentario from './components/Comentario'

class App extends Component{
  state ={
    comentarios: [
      {
        nome: 'João',
        email: 'joao@email.com',
        data: new Date(2020, 7, 22),
        mensagem: 'Olá, tudo bem?'
      },
      {
        nome: 'Maria',
        email: 'maria@email.com',
        data: new Date(2020, 7, 23),
        mensagem: 'Olá, tudo'
      }
    ],
    novoComentario: {
      nome: '',
      email: '',
      mensagem: '',
    }
  }
  adicionarComentario = evento =>{
    evento.preventDefault();
    const novoComentario = {...this.state.novoComentario, data: new Date()};

    this.setState({
      comentarios: [...this.state.comentarios, novoComentario], 
      novoComentario: {nome: '', email: '', mensagem: ''}
    })
  }
  removerComentario = comentario =>{
    let lista  = this.state.comentarios;
    lista = lista.filter(c => c !== comentario)
    this.setState({comentarios: lista})
  }
  digitacao = evento =>{
    const {name, value} = evento.target;
    this.setState({
      novoComentario: {...this.state.novoComentario, [name]: value}
    })
  }

  

  render(){
    return (
      <div className="App">
        <header className="App-header">
          <h1>Meu Projeto</h1>
          {this.state.comentarios.map((comentario, indice) => (
          <Comentario 
            key={indice}
            nome={comentario.nome} 
            email={comentario.email} 
            data={comentario.data}
            onRemove={this.removerComentario.bind(this, comentario)}
            >
              {comentario.mensagem}
          </Comentario>
          ))}
          <form method="post" className="Novo-Comentario">
            <h2>Adicionar comentário</h2>
            <div>
              <input type="text" required name="nome" onChange={this.digitacao} value={this.state.novoComentario.nome} placeholder="Digite seu nome"/>
            </div>
            <div>
              <input type="text" required name="email" onChange={this.digitacao} value={this.state.novoComentario.email} placeholder="Digite seu email"/>
            </div>
            <div>
              <textarea name="mensagem" required onChange={this.digitacao} value={this.state.novoComentario.mensagem} placeholder="Digite sua mensagem"/>
            </div>
            <button onClick={this.adicionarComentario}>Adicionar um comentário</button>
          </form>
        </header>
      </div>
    );
  }
  
}
  

export default App;
